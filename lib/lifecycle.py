import datetime
import logging
import signal
import threading
import time
import asyncio

from lib.lifecycle_helper import AsyncCallback, register_filter_thread, any_filter_thread_present, stop_all_filter_threads, all_filter_threads_alive


class Lifecycle:

    logger = logging.getLogger()

    def __init__(self):
        self.do_wait_for_sync = False
        self.delay = 0
        self.startup_function = None
        self.shutdown_function = None
        self.every_timers = []

        self.terminated_internally = False
        self.terminated_externally = False
        self.fatal_termination = False
        self._at_least_one_every = False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.logger.info(f"Bot initializing")

        if self.do_wait_for_sync:
            self._wait_for_init()

        if self.delay > 0:
            self.logger.info(f"Waiting for {self.delay} seconds of initial delay...")
            time.sleep(self.delay)

        if self.startup_function:
            self.logger.info("Executing keeper startup logic")
            self.startup_function()

        self._start_every_timers()
        self._main_loop()

        self.logger.info("Shutting down the bot")

        if any_filter_thread_present():
            self.logger.info("Waiting for all threads to terminate...")
            stop_all_filter_threads()

        if len(self.every_timers) > 0:
            self.logger.info("Waiting for outstanding timers to terminate...")
            for timer in self.every_timers:
                timer[1].wait()

        if self.shutdown_function:
            self.logger.info("Executing bot shutdown logic...")
            self.shutdown_function()
            self.logger.info("Shutdown logic finished")
        self.logger.info("Bot terminated")
        exit(10 if self.fatal_termination else 0)

    def _wait_for_init(self):
        i = 0

    def wait_for_sync(self, wait_for_sync: bool):
        assert(isinstance(wait_for_sync, bool))

        self.do_wait_for_sync = wait_for_sync

    def initial_delay(self, initial_delay: int):
        assert(isinstance(initial_delay, int))

        self.delay = initial_delay

    def on_startup(self, callback):
        assert(callable(callback))

        assert(self.startup_function is None)
        self.startup_function = callback

    def on_shutdown(self, callback):
        assert(callable(callback))

        assert(self.shutdown_function is None)
        self.shutdown_function = callback

    def terminate(self, message=None):
        if message is not None:
            self.logger.warning(message)

        self.terminated_internally = True

    def every(self, frequency_in_seconds: int, callback):
        self.every_timers.append((frequency_in_seconds, AsyncCallback(callback)))

    def _sigint_sigterm_handler(self, sig, frame):
        if self.terminated_externally:
            self.logger.warning("Graceful bot termination due to SIGINT/SIGTERM already in progress")
        else:
            self.logger.warning("Bot received SIGINT/SIGTERM signal, will terminate gracefully")
            self.terminated_externally = True

    def _start_every_timers(self):
        for timer in self.every_timers:
            self._start_every_timer(timer[0], timer[1])

        if len(self.every_timers) > 0:
            self.logger.info("Started timer(s)")

    def _start_every_timer(self, frequency_in_seconds: int, callback):
        def setup_timer(delay):
            timer = threading.Timer(delay, func)
            timer.daemon = True
            timer.start()

        def func():
            try:
                if not self.terminated_internally and not self.terminated_externally:
                    def on_start():
                        self.logger.debug(f"Processing the timer")

                    def on_finish():
                        self.logger.debug(f"Finished processing the timer")

                    if not callback.trigger(on_start, on_finish):
                        self.logger.debug(f"Ignoring timer as previous one is already running")
                else:
                    self.logger.debug(f"Ignoring timer as bot is already terminating")
            except:
                setup_timer(frequency_in_seconds)
                raise
            setup_timer(frequency_in_seconds)

        setup_timer(1)
        self._at_least_one_every = True

    def _main_loop(self):
        signal.signal(signal.SIGINT, self._sigint_sigterm_handler)
        signal.signal(signal.SIGTERM, self._sigint_sigterm_handler)

        while any_filter_thread_present() or self._at_least_one_every:
            time.sleep(1)

            if self.terminated_internally:
                self.logger.warning("Bot logic asked for termination, the bot will terminate")
                break

            if self.terminated_externally:
                self.logger.warning("The bot is terminating due do SIGINT/SIGTERM signal received")
                break

            if not all_filter_threads_alive():
                self.logger.fatal("One of filter threads is dead, the bot will terminate")
                self.fatal_termination = True
                break
