import sys
import argparse
import requests
import json
import logging
import time

import pandas
import numpy

from lib.lifecycle import Lifecycle


class TradingBot:

    DEFAULT_PAGE_SIZE = 1000

    def __init__(self, args: list, **kwargs):
        parser = argparse.ArgumentParser(prog='venv')
        parser.add_argument("--telegram-token", help="Telegram token for notifications of indicator changes", default="1032729626:AAFOT2cJW5-Y46MYr3NyII8dNXZFojX-d6E", type=str)
        parser.add_argument("--telegram-chat-id", help="Telegram channel to post notifications", default="-356466636", type=str)
        parser.add_argument("--default-span", help="# of days to weight average", default=55, type=int)

        self.arguments = parser.parse_args(args)

        logging.basicConfig(format='%(asctime)-15s %(levelname)-8s %(message)s', level=logging.INFO)

        self.btc_data = []

        self.last_indicator = None

        self.last_updated = 0

    def main(self):
        self.setup()

        with Lifecycle() as lifecycle:
            lifecycle.every(1 * 60 * 60, self.check_price)

    def check_price(self):
        last_btc_price_response = self.get_current_btc_price()
        latest_btc_time = last_btc_price_response["time"]
        latest_btc_close = last_btc_price_response["close"]

        if self.last_updated < latest_btc_time:
            self.btc_data.append(last_btc_price_response)
            self.last_updated = latest_btc_time

            btc_data_table = pandas.read_json(json.dumps(self.btc_data))
            data = pandas.Series(numpy.float32(btc_data_table['close']), index=btc_data_table['time'])
            ema = data.ewm(span=self.arguments.default_span).mean()

            last_ema = float(ema.tail(1).iloc[-1])
            this_indicator = latest_btc_close - last_ema

            logging.info(f"BTC price: ${'{:,.2f}'.format(latest_btc_close)} - EWA ${'{:,.2f}'.format(last_ema)}")

            if self.last_indicator is not None and self.has_indicator_changed(this_indicator):
                self.send_alert(latest_btc_close, last_ema)

            self.last_indicator = this_indicator

    def get_historical_btc_data(self):
        now = int(time.time())
        time_cutoff = now - (86400 * self.arguments.default_span)
        timestamp_offset = None
        up_to_date = False

        while not up_to_date:
            historical_data = self.request_btc_data(timestamp_offset)

            for row in historical_data:
                row_time = row["time"]

                self.btc_data.insert(0, row)

                if row_time < time_cutoff:
                    break

            last_row_time = self.btc_data[0]["time"]
            if last_row_time < time_cutoff:
                up_to_date = True
            else:
                timestamp_offset = last_row_time

        self.last_updated = self.btc_data[len(self.btc_data) - 1]["time"]

    def request_btc_data(self, offset=None, page_size=1000):
        offset_param = ""
        if offset:
            offset_param = f"&toTs={offset}"

        historical_btc_url = f"https://min-api.cryptocompare.com/data/v2/histohour?fsym=BTC&tsym=USD&limit={page_size}{offset_param}"
        historical_btc_request = requests.get(historical_btc_url)
        if historical_btc_request.status_code < 400:
            payload = json.loads(historical_btc_request.text)
            data = payload["Data"]["Data"]
            return data[::-1]
        else:
            logging.error(f"Error - {historical_btc_request.status_code}: {historical_btc_request.text}")
            return[]

    def setup(self):
        self.get_historical_btc_data()

    def get_current_btc_price(self, exchange="Coinbase"):
        exchange_param = ""
        if exchange:
            exchange_param = f"&e={exchange}"

        btc_request_url = f"https://min-api.cryptocompare.com/data/v2/histohour?fsym=BTC&tsym=USD&limit=1{exchange_param}"
        btc_request = requests.get(btc_request_url)
        if btc_request.status_code < 400:
            payload = json.loads(btc_request.text)
            price = payload["Data"]["Data"][1]
            return price
        else:
            return -1

    def has_indicator_changed(self, this_indicator):
        return self.last_indicator < 0 <= this_indicator or self.last_indicator >= 0 > this_indicator

    def send_alert(self, current_price, last_ema):
        title = "BTC price > EWA!"
        if current_price < last_ema:
            title = "BTC price < EWA!"

        display_btc = "{:,.2f}".format(current_price)
        display_ewa = "{:,.2f}".format(last_ema)
        message = f"{title}\nBTC:  ${display_btc} \nEWA: ${display_ewa}"

        bot_url = f"https://api.telegram.org/bot{self.arguments.telegram_token}/sendMessage?chat_id={self.arguments.telegram_chat_id}&text={message}"
        bot_request = requests.get(bot_url)
        if bot_request.status_code < 400:
            logging.error(f"Message sent")
        else:
            logging.error(f"{bot_request.status_code}: {bot_request.text}")


if __name__ == '__main__':
    TradingBot(sys.argv[1:]).main()
